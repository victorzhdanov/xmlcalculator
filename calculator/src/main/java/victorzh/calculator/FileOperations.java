package victorzh.calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.stream.*;
import javax.xml.transform.*;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides static methods, that performs following operations
 * with XML files:
 * <ul>
 * <li>Validate XML files;</li>
 * <li>Parse source XML file via StAX parser;</li>
 * <li>Create well-formed result XML file via StAX stream writer;</li>
 * <li>Get and set result file path.</li>
 * </ul>
 * 
 * @author Victor Zhdanov
 *
 */
class FileOperations {
	private static Path resultFilePath = Paths.get("result.xml");	
	private static final Logger log = LogManager.getLogger("victorzh.calculator");
	
	/**
	 * Validate that source XML file is valid to XML Schema file.
	 * 
	 * @param filePath Path to source XML file
	 * @param schemaPath Path to XML schema file
	 * @throws SAXException
	 * @throws IOException
	 */
	static void validateXML(final Path filePath, final Path schemaPath)
			throws SAXException, IOException {
		
		final SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		final Schema schema = factory.newSchema(schemaPath.toFile());
		final Validator validator = schema.newValidator();
		validator.validate(new StreamSource(filePath.toFile()));
	}
	
	/**
	 * Parse source XML file with StAX parser.
	 * 
	 * @param path Path to source XML file
	 * @return List of operators and operands from source XML file
	 * @throws XMLStreamException 
	 * @throws FileNotFoundException 
	 */
	static List<String> parseXML(final Path path)
			throws FileNotFoundException, XMLStreamException {
		
		final XMLInputFactory factory = XMLInputFactory.newInstance();
		final XMLStreamReader parser = factory.createXMLStreamReader(new FileReader(path.toFile()));
		
		final List<String> tokens = new ArrayList<>();
		
		while (parser.hasNext()) {
			int event = parser.next();
			
			if (event == XMLStreamConstants.START_ELEMENT) {
				if (parser.getLocalName().equals("operation"))
					tokens.add(parser.getAttributeValue(0).trim());
					
				else if (parser.getLocalName().equals("arg"))
					tokens.add(parser.getElementText().trim());
			}
		}
		
		if (!negativeOperandsFound(tokens)) return tokens;	
		else {
			log.warn("Negative operands found while source XML file parsing. {} thrown.", 
					XMLStreamException.class);
			
			throw new XMLStreamException(Messages.NEGATIVE_OPERANDS.getMessage());
		}
	}
	
	/**
	 * Write computation results to new well-formed XML file via StAX stream writer.
	 * 
	 * @param results List of computation results
	 * @throws XMLStreamException
	 * @throws TransformerException 
	 * @throws IOException 
	 */
	static void writeXML(final List<Double> results)
			throws XMLStreamException, TransformerException, IOException {
		
		if (results.isEmpty())
			throw new IllegalArgumentException(Messages.ILLEGAL_ARG_EXCEPTION.getMessage());
			
		final Writer out = new FileWriter(FileOperations.resultFilePath.toFile());

		final XMLOutputFactory factory = XMLOutputFactory.newInstance();
		final XMLStreamWriter writer = factory.createXMLStreamWriter(out);
		
		writer.writeStartDocument();
		writer.writeStartElement("simpleCalculator");
		writer.writeStartElement("expressionResults");
		for (Double d : results) {
			writer.writeStartElement("expressionResult");
			writer.writeStartElement("result");
			writer.writeCharacters(String.valueOf(d));
			writer.writeEndElement();
			writer.writeEndElement();
		}
		writer.writeEndDocument();	
		writer.close();
		
		transformXML();
	}
	
	/**
	 * Replace default outgoing file path
	 * 
	 * @param path New file path
	 * @return Result of setting new path (true if set, false otherwise).
	 */
	static boolean setFilePath(final Path path) {
		if (path.toString().matches(".*(\\.(xml|XML))")) {
			FileOperations.resultFilePath = path;
			return true;
		}
		else {
			log.warn("New result file path [{}] is not valid. No changes performed.", path.toString());
			return false;
		}
	}
	
	/**
	 * 
	 * @return Outgoing file path
	 */
	static Path getFilePath() {
		return FileOperations.resultFilePath;
	}
	
	//Makes outgoing XML file well-formed
	private static void transformXML()
			throws TransformerException, XMLStreamException, FileNotFoundException {
		
		final XMLInputFactory factory = XMLInputFactory.newInstance();
		final XMLStreamReader inReader = factory.createXMLStreamReader(new FileReader(resultFilePath.toFile()));
		final Source source = new StAXSource(inReader);
		final StreamResult outWriter = new StreamResult(new FileOutputStream(resultFilePath.toFile()));
		
		final TransformerFactory transformerFactory = TransformerFactory.newInstance();
		final Transformer transformer = transformerFactory.newTransformer();
		
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "5");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(source, outWriter);
		
		inReader.close();
	}
	
	static boolean negativeOperandsFound(final List<String> tokens) {
		for (String token : tokens)
			if (token.matches(Calculations.OPERAND_MASK))
				if (Integer.parseInt(token) < 0)
					return true;
		
		return false;
	}
}
