package victorzh.calculator;

enum Messages {
	MISSED_ARGS("No arguments found. Please specify as listed below:\n"
			+ "1. Source XML file - REQUIRED;\n"
			+ "2. Validation XML Schema - REQUIRED;\n"
			+ "3. New outgoing file name (path) - OPTIONAL.\n"
			+ "Program closed."),
	NEGATIVE_OPERANDS("One or more operands are negative."),
	IO_EXCEPTION("XML file or xsd schema (or both) is not found or can't be read."),
	SAX_EXCEPTION("Validation failed: "), //concatenated with e.getMessage 
	XML_STREAM_EXCEPTION("Problem with XML stream occurred: "), //concatenated with e.getMessage
	TRANSFORMER_EXCEPTION("Outgoing XML transformation problem."),
	ILLEGAL_ARG_EXCEPTION("Empty result list passed to writeXML method.");
	
	private String message;
	
	Messages(final String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return this.message;
	}
}
