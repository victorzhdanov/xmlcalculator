package victorzh.calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * At least two arguments needed to run the application:
 * <ul>
 * <li>Source XML file name (with extension);</li>
 * <li>XML Schema file name (with extension).</li>
 * </ul>
 * <p>
 * Argument order matters.
 * <p>
 * Application produces XML file with results of calculations. Both source and
 * outgoing file passes validation procedure. New outgoing XML file name 
 * (or full path) can be provided optionally as third argument. This new name
 * replaces default file name for current application launch.
 * <p>
 * All errors and warnings are logged to file (app.log), that is created and
 * stored in the same folder, where this program's jar is placed.
 * 
 * @author Victor Zhdanov
 *
 */
public class Calculator {
	private static final int MIN_ARGS_AMOUNT = 2;
	private static final Logger log = LogManager.getLogger("victorzh.calculator");
		
	public static void main(String[] args) {

		if (args.length < MIN_ARGS_AMOUNT) {
			System.out.println(Messages.MISSED_ARGS.getMessage());
			log.warn("No arguments found. Program closed.");
			return;
		}
		
		try {
			//Check first arg
			Path sourceFilePath;
			if (Files.exists(Paths.get(args[0])) && Files.isReadable(Paths.get(args[0]))) {
				sourceFilePath = Paths.get(args[0]);
			}
			else throw new IOException(Messages.IO_EXCEPTION.getMessage());
			
			//Check second arg
			Path schemaFilePath;
			if (Files.exists(Paths.get(args[1])) && Files.isReadable(Paths.get(args[1]))) {
				schemaFilePath = Paths.get(args[1]);
			}
			else throw new IOException(Messages.IO_EXCEPTION.getMessage());
			
			//Third arg (if exists) is set as new outgoing XML file path
			if (args.length > MIN_ARGS_AMOUNT) {
				Path newOutgoingPath = Files.createFile(Paths.get(args[2]));
				FileOperations.setFilePath(newOutgoingPath);
			}

			//Process files and calculate
			FileOperations.validateXML(sourceFilePath, schemaFilePath);
			List<String> operators = FileOperations.parseXML(sourceFilePath);
			List<Double> result = Calculations.evaluate(operators); 
			FileOperations.writeXML(result);
			FileOperations.validateXML(FileOperations.getFilePath(), schemaFilePath);
		}
		catch (IOException e) {
			System.out.println(Messages.IO_EXCEPTION.getMessage());
			log.error("{} {} {}", e.getClass(), e.getCause(), e.getMessage());
		} 
		catch (SAXException e) {
			System.out.println(Messages.SAX_EXCEPTION.getMessage() + e.getMessage());
			log.error("{} {} {}", e.getClass(), e.getCause(), e.getMessage());
		} 
		catch (XMLStreamException e) {
			System.out.println(Messages.XML_STREAM_EXCEPTION.getMessage() + e.getMessage());
			log.error("{} {} {}", e.getClass(), e.getCause(), e.getMessage());
		} 
		catch (TransformerException e) {
			System.out.println(Messages.TRANSFORMER_EXCEPTION.getMessage());
			log.error("{} {} {}", e.getClass(), e.getCause(), e.getMessage());
		}
		catch (IllegalArgumentException e) {
			System.out.println(Messages.ILLEGAL_ARG_EXCEPTION.getMessage());
			log.error("{} {} {}", e.getClass(), e.getCause(), e.getMessage());
		}
	}
}
