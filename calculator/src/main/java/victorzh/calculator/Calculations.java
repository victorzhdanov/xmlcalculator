package victorzh.calculator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;


/**
 * This class provides single public static method, that evaluates result of
 * arithmetic operations with values, excluded from source XML file.
 * <p>
 * List of operators and operands is treated as one expression, written in
 * Polish notation.
 * 
 * @author Victor Zhdanov
 *
 */
class Calculations {
	private static final int OPERANDS_MAX_VALUE = 2;
	static final String OPERATOR_MASK = "(SUB|SUM|MUL|DIV)";
	static final String OPERAND_MASK = "-?[0-9]+";
	
	/**
	 * This static method provides result (as <code>java.lang.Double</code>),
	 * that can be later used to prepare outgoing XML file.
	 * 
	 * @param operatorsList List of operators and values, extracted from source XML file
	 */
	static List<Double> evaluate(final List<String> operatorsList) {
		
		//Deque for full operators and operands list from source
		final Deque<String> sourceOperators = new ArrayDeque<>();
		operatorsList.forEach(sourceOperators::addLast);

		/*
		* in line 34 previously was:
		* for (String s : operatorsList)
		* 	sourceOperators.addLast(s);
		*/
		
		//Hierarchy of deque's for calculations
		final Deque<String> operators = new ArrayDeque<>();
		final Deque<Integer> operands = new ArrayDeque<>();
		final Deque<Double> preResults = new ArrayDeque<>();
		final Deque<Double> results = new ArrayDeque<>();
		final List<Double> finalResult = new ArrayList<>();
		
		while (!sourceOperators.isEmpty()) {
			
			if (sourceOperators.getFirst().matches(OPERATOR_MASK))
				operators.addFirst(sourceOperators.removeFirst());
					
			if (sourceOperators.getFirst().matches(OPERAND_MASK))
				operands.addFirst(Integer.parseInt(sourceOperators.removeFirst()));
			
			if (operands.size() == OPERANDS_MAX_VALUE) {
				final double x = (double)operands.removeLast();
				final double y = (double)operands.removeFirst();
				final String operator = operators.removeFirst();
				final double result = Calculations.calculate(x, y, operator);
				preResults.addFirst(result);
			}
			
			if (preResults.size() == OPERANDS_MAX_VALUE) {
				final double x = preResults.removeLast();
				final double y = preResults.removeFirst();
				final String operator = operators.removeFirst();
				final double result = Calculations.calculate(x, y, operator);
				results.addFirst(result);
			}
			
			if (results.size() == OPERANDS_MAX_VALUE) {
				final double x = results.removeLast();
				final double y = results.removeFirst();
				final String operator = operators.removeFirst();
				final double result = Calculations.calculate(x, y, operator);
				finalResult.add(result);
			}
		}
		
		return finalResult;
	}
	
	private static double calculate(final double x, final double y, final String operator) {
		double result = 0.0;

		switch(operator) {
		case "SUB":
			result = x - y;
			break;
		case "SUM": 
			result = x + y;
			break;
		case "MUL": 
			result = x * y;
			break;
		case "DIV": 
			result = x / y;
			break;
		default: //never happens while this method consumes operators after comparing it with mask
		}
		
		return result;
	}
}
