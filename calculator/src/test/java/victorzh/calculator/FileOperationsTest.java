package victorzh.calculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class FileOperationsTest {
	private Path filePathHolder;
	private Path badSchemaFile;
	private Path referenceSchemaFile;
	private Path testSourceFile;
	private Path tempFile;
	private List<String> testSourceList;
	@SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
	private List<Double> testResultList;
	private List<Double> emptyTestResultList;

	@Before
	public void setUp() throws Exception {
		this.filePathHolder = FileOperations.getFilePath();
		this.badSchemaFile = Paths.get("resources/BrokenSchema.xsd");
		this.referenceSchemaFile = Paths.get("resources/ReferenceSchema.xsd");
		this.testSourceFile = Paths.get("resources/TestSource.xml");
		this.tempFile = Paths.get("E:/git/calculatorLocal/tempFile.xml");
		
		this.testSourceList = new ArrayList<>(30);
		this.testSourceList.add("MUL");
		this.testSourceList.add("SUB");
		this.testSourceList.add("SUM");
		this.testSourceList.add("2");
		this.testSourceList.add("4");
		this.testSourceList.add("DIV");
		this.testSourceList.add("1");
		this.testSourceList.add("4");
		this.testSourceList.add("SUB");
		this.testSourceList.add("SUM");
		this.testSourceList.add("12");
		this.testSourceList.add("55");
		this.testSourceList.add("MUL");
		this.testSourceList.add("123");
		this.testSourceList.add("4");
		this.testSourceList.add("SUB");
		this.testSourceList.add("MUL");
		this.testSourceList.add("DIV");
		this.testSourceList.add("211");
		this.testSourceList.add("114");
		this.testSourceList.add("DIV");
		this.testSourceList.add("13");
		this.testSourceList.add("41");
		this.testSourceList.add("SUB");
		this.testSourceList.add("SUM");
		this.testSourceList.add("123");
		this.testSourceList.add("1235");
		this.testSourceList.add("MUL");
		this.testSourceList.add("55");
		this.testSourceList.add("1111");
		
		this.testResultList = new ArrayList<>(2);
		this.testResultList.add(-2443.75);
		this.testResultList.add(59747.58686350021);
		this.emptyTestResultList = new ArrayList<>();
	}

	@After
	public void tearDown() throws Exception {
		FileOperations.setFilePath(this.filePathHolder);
		this.testSourceList.clear();
		this.testResultList.clear();
		try {
			Files.delete(this.tempFile);
		}
		catch (IOException ignored) {}
	}

	@Test
	public void testValidateXML() {
		try {
			FileOperations.validateXML(testSourceFile, badSchemaFile);
		}
		catch (IOException | SAXException e) {
			fail("No exceptions expected: testValidateXML");
		}
	}

	@Test
	public void testParseXML() {
		try {
			assertArrayEquals(testSourceList.toArray(), FileOperations.parseXML(testSourceFile).toArray());
		} catch (FileNotFoundException | XMLStreamException e) {
			fail("No exceptions expected: testParseXML");
		}
	}

	@Test
	public void testWriteXML() {
		try {
			if (!Files.exists(this.tempFile))
				Files.createFile(this.tempFile);
			
			FileOperations.setFilePath(this.tempFile);
			List<Double> testResult = Calculations.evaluate(this.testSourceList);
			
			FileOperations.writeXML(testResult);
		} catch (XMLStreamException | TransformerException | IOException e) {
			fail("No exceptions expected: testWriteXML");
		}
		try {
			FileOperations.validateXML(FileOperations.getFilePath(), this.referenceSchemaFile);
		} catch (SAXException | IOException e) {
			fail("No exceptions expected: testWriteXML");
		}
		
		//Writing empty list with no results to file
		try {
			FileOperations.writeXML(this.emptyTestResultList);
			fail("Exception expected in writeXML with empty list, but it is missing.");
		}
		catch (IllegalArgumentException ignored) {}
		catch (XMLStreamException | TransformerException | IOException e) {
			fail("Wrong exception raised in writeXML with empty list");
		}
	}

	@Test
	public void testSetFilePath() {
		assertTrue(FileOperations.setFilePath(Paths.get("something.xml")));
		assertTrue(FileOperations.setFilePath(Paths.get("something.XML")));
		assertFalse(FileOperations.setFilePath(Paths.get("something.txt")));
		assertFalse(FileOperations.setFilePath(Paths.get("something.xm")));
		assertFalse(FileOperations.setFilePath(Paths.get("something.xmll")));
	}

	@Test
	public void testGetFilePath() {
		assertNotNull(FileOperations.getFilePath());
		
		Pattern p = Pattern.compile(".*(\\.(xml|XML))");
		Matcher matcher = p.matcher(FileOperations.getFilePath().toString());
		assertTrue(matcher.matches());
	}
	
	@Test
	public void testNegativeOperandsFound() {
		testSourceList.set(7, "-4");
		assertTrue(FileOperations.negativeOperandsFound(testSourceList));
		testSourceList.set(7, "4");
		assertFalse(FileOperations.negativeOperandsFound(testSourceList));	
	}
}
