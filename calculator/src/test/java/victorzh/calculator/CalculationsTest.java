package victorzh.calculator;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class CalculationsTest {
	private List<String> testSource;
	private List<Double> testResult;

	@Before
	public void setUp() throws Exception {
		this.testSource = new ArrayList<>(30);
		this.testSource.add("MUL");
		this.testSource.add("SUB");
		this.testSource.add("SUM");
		this.testSource.add("2");
		this.testSource.add("4");
		this.testSource.add("DIV");
		this.testSource.add("1");
		this.testSource.add("4");
		this.testSource.add("SUB");
		this.testSource.add("SUM");
		this.testSource.add("12");
		this.testSource.add("55");
		this.testSource.add("MUL");
		this.testSource.add("123");
		this.testSource.add("4");
		this.testSource.add("SUB");
		this.testSource.add("MUL");
		this.testSource.add("DIV");
		this.testSource.add("211");
		this.testSource.add("114");
		this.testSource.add("DIV");
		this.testSource.add("13");
		this.testSource.add("41");
		this.testSource.add("SUB");
		this.testSource.add("SUM");
		this.testSource.add("123");
		this.testSource.add("1235");
		this.testSource.add("MUL");
		this.testSource.add("55");
		this.testSource.add("1111");
		
		this.testResult = new ArrayList<>(2);
		this.testResult.add(-2443.75);
		this.testResult.add(59747.58686350021);	
	}

	@After
	public void tearDown() throws Exception {
		this.testSource.clear();
		this.testResult.clear();
	}

	@Test
	public void testEvaluate() {
		assertEquals(testResult, Calculations.evaluate(testSource));	
	}
	
	@Test
	public void testOperatorMaskMatch() {	
		String sub = "SUB"; //<xs:enumeration value="SUB"/>
		String sum = "SUM"; //<xs:enumeration value="SUM"/>
		String mul = "MUL"; //<xs:enumeration value="MUL"/>
		String div = "DIV"; //<xs:enumeration value="DIV"/>
		
		assertTrue(sub.matches(Calculations.OPERATOR_MASK));
		assertTrue(sum.matches(Calculations.OPERATOR_MASK));
		assertTrue(mul.matches(Calculations.OPERATOR_MASK));
		assertTrue(div.matches(Calculations.OPERATOR_MASK));
	}
	
	@Test
	public void testOperandMaskMatch() {
		for (int step = 0; (step >= 0 && step < Integer.MAX_VALUE - 1); step++) {
			Random r = new Random();
			int skip = r.nextInt(1000) * 100; //Skipping random ranges to reduce test time
			String s = String.valueOf(step);
			assertTrue(s.matches(Calculations.OPERAND_MASK));
			step += skip;
		}
	}
}
